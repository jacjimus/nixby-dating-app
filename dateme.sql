-- MySQL dump 10.15  Distrib 10.0.36-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: dateme
-- ------------------------------------------------------
-- Server version	10.0.36-MariaDB-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abuse_reports`
--

DROP TABLE IF EXISTS `abuse_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abuse_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` varchar(255) CHARACTER SET latin1 NOT NULL,
  `reporting_user_id` int(11) NOT NULL,
  `reported_user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abuse_reports`
--

LOCK TABLES `abuse_reports` WRITE;
/*!40000 ALTER TABLE `abuse_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `abuse_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `html_code` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blocked_user`
--

DROP TABLE IF EXISTS `blocked_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blocked_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `block_user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blocked_user`
--

LOCK TABLES `blocked_user` WRITE;
/*!40000 ALTER TABLE `blocked_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `blocked_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bots`
--

DROP TABLE IF EXISTS `bots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(64) NOT NULL,
  `gender` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `age` int(11) NOT NULL,
  `joining` datetime NOT NULL,
  `photo_id` varchar(255) NOT NULL,
  `enable` int(11) NOT NULL,
  `user_ids` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bots`
--

LOCK TABLES `bots` WRITE;
/*!40000 ALTER TABLE `bots` DISABLE KEYS */;
INSERT INTO `bots` VALUES (11,'Claire Jones','Swanseacity111',2,'2018-10-04 22:29:08','2018-10-04 22:29:08',37,'2014-01-01 00:00:00','1538692147368295bb69433ea97c',1,'a:0:{}'),(12,'kirsty','Swanseacity111',2,'2018-10-04 22:34:17','2018-10-04 22:34:17',25,'2015-09-16 00:00:00','1538692457192465bb69569e01be',1,'a:0:{}'),(13,'Stacey Williams','Swanseacity111',2,'2018-10-04 22:34:57','2018-10-04 22:34:57',36,'2018-09-16 00:00:00','1538692497165565bb69591ad1fd',1,'a:0:{}'),(14,'Tasha James','Swanseacity111',2,'2018-10-04 22:36:01','2018-10-04 22:36:01',34,'2017-10-17 00:00:00','1538692561171475bb695d1d0077',1,'a:0:{}'),(15,'Sara Thomas','Swanseacity111',2,'2018-10-04 22:37:01','2018-10-04 22:37:01',38,'2016-05-18 00:00:00','1538692621190555bb6960d3f055',1,'a:0:{}'),(16,'chloe','Swanseacity111',2,'2018-10-04 22:37:31','2018-10-04 22:37:31',47,'2018-08-14 00:00:00','1538692651227425bb6962b8e85b',1,'a:0:{}'),(17,'layla','Swanseacity111',2,'2018-10-05 08:01:51','2018-10-05 08:01:51',24,'2018-10-15 00:00:00','1538726511146395bb71a6fb9c3c',1,'a:0:{}'),(18,'Leah','Swanseacity111',2,'2018-10-05 08:03:45','2018-10-05 08:03:45',39,'2017-10-17 00:00:00','1538726625242025bb71ae192e28',1,'a:0:{}'),(19,'Jackie','Swanseacity111',2,'2018-10-05 08:04:27','2018-10-05 08:04:27',41,'2018-05-17 00:00:00','1538726667264985bb71b0ba245b',1,'a:0:{}'),(20,'Angela','Swanseacity111',2,'2018-10-05 08:05:56','2018-10-05 08:05:56',31,'2017-09-17 00:00:00','1538726756166025bb71b640b65b',1,'a:0:{}'),(21,'Meg','Swanseacity111',2,'2018-10-05 08:06:58','2018-10-05 08:06:58',24,'2018-06-17 00:00:00','1538726818273925bb71ba287d3a',1,'a:0:{}'),(22,'Jolene','Swanseacity111',2,'2018-10-05 08:08:36','2018-10-05 08:08:36',43,'2016-11-19 00:00:00','1538726916300075bb71c0425660',1,'a:0:{}'),(23,'Samantha','Swanseacity111',2,'2018-10-05 08:09:37','2018-10-05 08:09:37',21,'2014-01-01 00:00:00','1538726977290615bb71c41212c6',1,'a:0:{}'),(26,'lorna','Swanseacity111',2,'2018-10-05 08:12:59','2018-10-05 08:12:59',39,'2018-10-05 00:00:00','1538727179183475bb71d0ba9276',1,'a:0:{}'),(27,'jade collins','Swanseacity111',2,'2018-10-05 08:14:24','2018-10-05 08:14:24',20,'2018-10-04 00:00:00','1538727264201505bb71d602015c',1,'a:0:{}'),(28,'Claire Morris','Swanseacity111',2,'2018-10-05 08:15:11','2018-10-05 08:15:11',19,'2018-08-04 00:00:00','1538727311171425bb71d8fdaf7f',1,'a:0:{}'),(29,'Nicola ','Swanseacity111',2,'2018-10-05 08:16:05','2018-10-05 08:16:05',37,'2018-10-04 00:00:00','1538727365197595bb71dc52c3b9',1,'a:0:{}'),(30,'Anna Griffiths ','Swanseacity111',2,'2018-10-05 08:17:11','2018-10-05 08:17:11',30,'2018-08-17 00:00:00','1538727431159635bb71e07a5117',1,'a:0:{}');
/*!40000 ALTER TABLE `bots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chats`
--

DROP TABLE IF EXISTS `chats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_user` int(11) NOT NULL,
  `to_user` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `notify_status` int(11) NOT NULL,
  `message` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=202 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chats`
--

LOCK TABLES `chats` WRITE;
/*!40000 ALTER TABLE `chats` DISABLE KEYS */;
/*!40000 ALTER TABLE `chats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `contact` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credithistory`
--

DROP TABLE IF EXISTS `credithistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credithistory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `type` varchar(200) CHARACTER SET latin1 NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `transaction_id` varchar(64) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=359 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credithistory`
--

LOCK TABLES `credithistory` WRITE;
/*!40000 ALTER TABLE `credithistory` DISABLE KEYS */;
INSERT INTO `credithistory` VALUES (356,21110,11,5,'app.userlogin','2018-10-04 20:58:40','2018-10-04 20:58:40','N/A'),(357,21110,16,5,'app.userlogin','2018-10-05 08:18:41','2018-10-05 08:18:41','N/A'),(358,21112,10,5,'app.userlogin','2018-10-05 09:19:25','2018-10-05 09:19:25','N/A');
/*!40000 ALTER TABLE `credithistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credits`
--

DROP TABLE IF EXISTS `credits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21047 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credits`
--

LOCK TABLES `credits` WRITE;
/*!40000 ALTER TABLE `credits` DISABLE KEYS */;
INSERT INTO `credits` VALUES (21042,21095,6,'2018-10-04 19:53:04','2018-10-04 19:53:04'),(21043,21109,6,'2018-10-04 20:55:13','2018-10-04 20:55:13'),(21044,21110,16,'2018-10-04 20:57:38','2018-10-05 08:18:41'),(21045,21111,5,'2018-10-05 08:21:17','2018-10-05 08:21:17'),(21046,21112,10,'2018-10-05 08:39:32','2018-10-05 09:19:25');
/*!40000 ALTER TABLE `credits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `czars`
--

DROP TABLE IF EXISTS `czars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `czars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `last_ip` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `czars`
--

LOCK TABLES `czars` WRITE;
/*!40000 ALTER TABLE `czars` DISABLE KEYS */;
INSERT INTO `czars` VALUES (6,'root','$2a$08$rMEwcc5SE3MyJMhzoROvdOYuVqSYg37iJPlmfOIcuu0V/ESy8CD32','2018-10-04 19:51:46','2018-10-04 19:51:46','0000-00-00 00:00:00',''),(7,'cookstar4eva','$2a$08$Q9qPt1/F771W.oF211guIepvvw4lVJZGaKEYKD83.xiYsCXnhvnAW','2018-10-04 22:01:52','2018-10-05 08:40:38','2018-10-05 08:40:38','197.237.220.126');
/*!40000 ALTER TABLE `czars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encounters`
--

DROP TABLE IF EXISTS `encounters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encounters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_user` int(11) NOT NULL,
  `to_user` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encounters`
--

LOCK TABLES `encounters` WRITE;
/*!40000 ALTER TABLE `encounters` DISABLE KEYS */;
/*!40000 ALTER TABLE `encounters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_user` int(10) unsigned NOT NULL,
  `to_user` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gifts`
--

DROP TABLE IF EXISTS `gifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `icon_id` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gifts`
--

LOCK TABLES `gifts` WRITE;
/*!40000 ALTER TABLE `gifts` DISABLE KEYS */;
/*!40000 ALTER TABLE `gifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interest_categories`
--

DROP TABLE IF EXISTS `interest_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interest_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `code` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interest_categories`
--

LOCK TABLES `interest_categories` WRITE;
/*!40000 ALTER TABLE `interest_categories` DISABLE KEYS */;
INSERT INTO `interest_categories` VALUES (7,'interests','interests','2018-10-04 21:02:39','2018-10-04 21:02:39');
/*!40000 ALTER TABLE `interest_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interests`
--

DROP TABLE IF EXISTS `interests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interests` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interests`
--

LOCK TABLES `interests` WRITE;
/*!40000 ALTER TABLE `interests` DISABLE KEYS */;
INSERT INTO `interests` VALUES (11,'interests','sport','2018-10-04 21:02:44','2018-10-04 21:02:44'),(12,'interests','music','2018-10-04 21:02:47','2018-10-04 21:02:47'),(13,'interests','shopping','2018-10-04 21:02:50','2018-10-04 21:02:50'),(14,'interests','movies','2018-10-04 21:02:53','2018-10-04 21:02:53'),(15,'interests','social media','2018-10-04 21:02:58','2018-10-04 21:02:58'),(16,'interests','knitting','2018-10-04 21:03:04','2018-10-04 21:03:04'),(17,'interests','reading','2018-10-04 21:03:08','2018-10-04 21:03:08'),(18,'interests','gardening','2018-10-04 21:03:14','2018-10-04 21:03:14');
/*!40000 ALTER TABLE `interests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetme`
--

DROP TABLE IF EXISTS `meetme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetme` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_user` int(11) NOT NULL,
  `to_user` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetme`
--

LOCK TABLES `meetme` WRITE;
/*!40000 ALTER TABLE `meetme` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_user` int(11) NOT NULL,
  `to_user` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `notify_status` int(11) NOT NULL,
  `type` varchar(200) CHARACTER SET latin1 NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=377 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `credits` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages`
--

LOCK TABLES `packages` WRITE;
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photo_comments`
--

DROP TABLE IF EXISTS `photo_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `album_id` int(11) unsigned NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo_comments`
--

LOCK TABLES `photo_comments` WRITE;
/*!40000 ALTER TABLE `photo_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `photo_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photo_rater`
--

DROP TABLE IF EXISTS `photo_rater`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo_rater` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `photo_id` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo_rater`
--

LOCK TABLES `photo_rater` WRITE;
/*!40000 ALTER TABLE `photo_rater` DISABLE KEYS */;
/*!40000 ALTER TABLE `photo_rater` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photo_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` float NOT NULL,
  `no_users` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (72,'1538692107347635bb6940b38f85',0,0,0,'2018-10-04 22:28:27','2018-10-04 22:28:27'),(73,'1538692147368295bb69433ea97c',0,0,0,'2018-10-04 22:29:08','2018-10-04 22:29:08'),(74,'1538692457192465bb69569e01be',0,0,0,'2018-10-04 22:34:17','2018-10-04 22:34:17'),(75,'1538692497165565bb69591ad1fd',0,0,0,'2018-10-04 22:34:57','2018-10-04 22:34:57'),(76,'1538692561171475bb695d1d0077',0,0,0,'2018-10-04 22:36:01','2018-10-04 22:36:01'),(77,'1538692621190555bb6960d3f055',0,0,0,'2018-10-04 22:37:01','2018-10-04 22:37:01'),(78,'1538692651227425bb6962b8e85b',0,0,0,'2018-10-04 22:37:31','2018-10-04 22:37:31'),(79,'1538726511146395bb71a6fb9c3c',0,0,0,'2018-10-05 08:01:51','2018-10-05 08:01:51'),(80,'1538726625242025bb71ae192e28',0,0,0,'2018-10-05 08:03:45','2018-10-05 08:03:45'),(81,'1538726667264985bb71b0ba245b',0,0,0,'2018-10-05 08:04:27','2018-10-05 08:04:27'),(82,'1538726756166025bb71b640b65b',0,0,0,'2018-10-05 08:05:56','2018-10-05 08:05:56'),(83,'1538726818273925bb71ba287d3a',0,0,0,'2018-10-05 08:06:58','2018-10-05 08:06:58'),(84,'1538726916300075bb71c0425660',0,0,0,'2018-10-05 08:08:36','2018-10-05 08:08:36'),(85,'1538726977290615bb71c41212c6',0,0,0,'2018-10-05 08:09:37','2018-10-05 08:09:37'),(86,'1538727034171425bb71c7aa4178',0,0,0,'2018-10-05 08:10:34','2018-10-05 08:10:34'),(87,'1538727088201505bb71cb08e9fc',0,0,0,'2018-10-05 08:11:28','2018-10-05 08:11:28'),(88,'1538727179183475bb71d0ba9276',0,0,0,'2018-10-05 08:12:59','2018-10-05 08:12:59'),(89,'1538727264201505bb71d602015c',0,0,0,'2018-10-05 08:14:24','2018-10-05 08:14:24'),(90,'1538727311171425bb71d8fdaf7f',0,0,0,'2018-10-05 08:15:11','2018-10-05 08:15:11'),(91,'1538727365197595bb71dc52c3b9',0,0,0,'2018-10-05 08:16:05','2018-10-05 08:16:05'),(92,'1538727431159635bb71e07a5117',0,0,0,'2018-10-05 08:17:11','2018-10-05 08:17:11');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `relationshipstatus` int(11) NOT NULL DEFAULT '0',
  `bodytype` int(11) NOT NULL DEFAULT '0',
  `haircolor` int(11) NOT NULL DEFAULT '0',
  `eyecolor` int(11) NOT NULL DEFAULT '0',
  `living` int(11) NOT NULL DEFAULT '0',
  `smoking` int(11) NOT NULL DEFAULT '0',
  `drinking` int(11) NOT NULL DEFAULT '0',
  `education` int(11) NOT NULL DEFAULT '0',
  `children` int(11) NOT NULL DEFAULT '0',
  `aboutme` varchar(255) CHARACTER SET latin1 NOT NULL,
  `interestedin` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `popularity` int(11) NOT NULL,
  `whyamihere` int(11) NOT NULL,
  `preferred_gender` int(1) NOT NULL,
  `preferred_age` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21061 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (21056,21095,1,0,0,0,0,0,0,0,0,'','','2018-10-04 19:53:04','2018-10-04 19:53:04',0,1,2,2),(21057,21109,1,0,0,0,0,0,0,0,0,'','','2018-10-04 20:55:13','2018-10-04 20:55:13',0,1,2,3),(21058,21110,1,0,0,0,0,0,0,0,0,'','','2018-10-04 20:57:38','2018-10-05 08:19:09',0,1,2,2),(21059,21111,1,0,0,0,0,0,0,0,0,'','','2018-10-05 08:21:17','2018-10-05 08:21:17',0,1,2,1),(21060,21112,1,4,1,2,5,1,2,4,1,'','','2018-10-05 08:39:32','2018-10-05 09:24:59',0,2,2,3);
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reward_packages`
--

DROP TABLE IF EXISTS `reward_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reward_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` varchar(255) CHARACTER SET latin1 NOT NULL,
  `credits` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reward_packages`
--

LOCK TABLES `reward_packages` WRITE;
/*!40000 ALTER TABLE `reward_packages` DISABLE KEYS */;
INSERT INTO `reward_packages` VALUES (1,'user.visit.profile',20,1,'0000-00-00 00:00:00','2014-03-29 21:42:31'),(2,'user.profile.visited',10,1,'0000-00-00 00:00:00','2014-03-29 21:42:31'),(3,'message.request.send',10,1,'0000-00-00 00:00:00','2014-03-29 21:42:31'),(5,'user.encounter.yes',20,1,'0000-00-00 00:00:00','2014-03-29 21:42:31'),(6,'album.photo.upload',10,1,'0000-00-00 00:00:00','2014-03-29 21:42:31'),(7,'profile.photo.upload',15,1,'0000-00-00 00:00:00','2014-03-29 21:42:31'),(8,'user.login',5,1,'0000-00-00 00:00:00','2014-03-29 21:42:31');
/*!40000 ALTER TABLE `reward_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(40) CHARACTER SET latin1 NOT NULL,
  `last_activity` int(11) NOT NULL,
  `data` text CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('UyJ2OALW2J6ViAjZ2ZozGhIjSQc8R6nMOQE4m8Aq',1538731664,'a:5:{s:5:\":new:\";a:0:{}s:5:\":old:\";a:0:{}s:10:\"csrf_token\";s:40:\"ZSYBtAaGiFX3xNKu1n1vJZjGB0Tzpx5hDZHMUYF6\";s:20:\"session_captcha_hash\";s:60:\"$2a$08$dtGL1RQK7QIj4rhukbz1auN1CUrPxCXeYT7PE4gmdq8vJjAtbQgQ.\";s:12:\"czar_session\";O:4:\"Czar\":5:{s:10:\"attributes\";a:7:{s:2:\"id\";i:7;s:8:\"username\";s:12:\"cookstar4eva\";s:8:\"password\";s:60:\"$2a$08$Q9qPt1/F771W.oF211guIepvvw4lVJZGaKEYKD83.xiYsCXnhvnAW\";s:10:\"created_at\";s:19:\"2018-10-04 22:01:52\";s:10:\"updated_at\";O:8:\"DateTime\":3:{s:4:\"date\";s:26:\"2018-10-05 08:40:38.000000\";s:13:\"timezone_type\";i:3;s:8:\"timezone\";s:3:\"UTC\";}s:10:\"last_login\";s:19:\"2018-10-05 08:40:38\";s:7:\"last_ip\";s:15:\"197.237.220.126\";}s:8:\"original\";a:7:{s:2:\"id\";i:7;s:8:\"username\";s:12:\"cookstar4eva\";s:8:\"password\";s:60:\"$2a$08$Q9qPt1/F771W.oF211guIepvvw4lVJZGaKEYKD83.xiYsCXnhvnAW\";s:10:\"created_at\";s:19:\"2018-10-04 22:01:52\";s:10:\"updated_at\";r:12;s:10:\"last_login\";s:19:\"2018-10-05 08:40:38\";s:7:\"last_ip\";s:15:\"197.237.220.126\";}s:13:\"relationships\";a:0:{}s:6:\"exists\";b:1;s:8:\"includes\";a:0:{}}}',0),('jwLEmO8logiz8kdMgyjhJRLpxeiDyzUWS6EFXobN',1538728788,'a:3:{s:5:\":new:\";a:0:{}s:5:\":old:\";a:0:{}s:10:\"csrf_token\";s:40:\"TE9vDlGRMhb6yZAfgoBDUReQ3V5nLvIGLqfunDLl\";}',0),('kqCJKQcG4XEZY7n6qREiouAccJgW1ZfLJxq8TpaL',1538728789,'a:3:{s:5:\":new:\";a:0:{}s:5:\":old:\";a:0:{}s:10:\"csrf_token\";s:40:\"qR6319U2hDrDZoSmxPp0T94w9LKdpalUrGj0QzMq\";}',0),('U2hwKJs8yjmJixPc9OLhP8cRe26WUXIWCbU6H4L7',1538728849,'a:3:{s:5:\":new:\";a:0:{}s:5:\":old:\";a:0:{}s:10:\"csrf_token\";s:40:\"TZcGe4CzMz8mRsEfB0Fn5LThJhvpfbDGeiSkdDgX\";}',0),('d0bHAzPo9mxWTRYElbYdG3FXNIx1MenayQvLnh6B',1538728895,'a:3:{s:5:\":new:\";a:0:{}s:5:\":old:\";a:0:{}s:10:\"csrf_token\";s:40:\"Nn2tkShY4oIXZ31xOEADa3XZpelKcEtUnEQ0Vavs\";}',0),('1FtE8mWbVSHl2kHQwWBkFhdBDZjnua7Om1NLM6bx',1538728949,'a:3:{s:5:\":new:\";a:0:{}s:5:\":old:\";a:0:{}s:10:\"csrf_token\";s:40:\"XplFLrPA9reBZVphGnqiQmRSe47dE4FkznEeXQxx\";}',0),('12amjQ68iED5yOIRT2uptUzvINFdjwHAVwM2sT95',1538731733,'a:4:{s:5:\":new:\";a:0:{}s:5:\":old:\";a:0:{}s:10:\"csrf_token\";s:40:\"ghmUbsI7Ts5AK6YugcVH9vwk8QGt41oQW7ZyBqcn\";s:20:\"session_captcha_hash\";s:60:\"$2a$08$y/TORFka1AK62vepYrnbSumQTEuzUL3.LVPLWAyMvryZR3zgqueNC\";}',21112),('i3B8tNpFOL4U0uGunqPxa469re3Sdvf4SG1vcU9Z',1538729563,'a:3:{s:5:\":new:\";a:0:{}s:5:\":old:\";a:0:{}s:10:\"csrf_token\";s:40:\"uIufvbggUfngw7Qyc9p1OE4nhdM1W64PRZQq7DeT\";}',0),('EGynP0zN6XbcsHTWee9vqRdP2IW7Z4JvfflDpjlF',1538731929,'a:4:{s:5:\":new:\";a:0:{}s:5:\":old:\";a:0:{}s:10:\"csrf_token\";s:40:\"g0sBGknbAhAczClXQOY6REeaXQqI2P5KK5O5KNOR\";s:20:\"session_captcha_hash\";s:60:\"$2a$08$AoqeUavdtf3atc42.hR3/OgbFcYgGlv3drTmi5tNNaiZjmIzE9V5W\";}',0),('6SZGBq7IVOHIDdZ96Aouo8fxUNi7VnQtleLMr7L0',1538728788,'a:3:{s:5:\":new:\";a:0:{}s:5:\":old:\";a:0:{}s:10:\"csrf_token\";s:40:\"AopUA8wpDeC2m2TjB2MyhLSGOWmxZ6EVquej8raJ\";}',0);
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `value` varchar(200) CHARACTER SET latin1 NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=149 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (4,'language','en','2013-09-07 15:50:28','2013-12-09 02:09:55'),(5,'title','nixby','2013-09-07 16:12:40','2018-10-04 21:00:44'),(8,'fbid','','2013-10-02 13:00:47','2014-03-27 22:55:33'),(9,'fbsecret','','2013-10-02 13:00:47','2014-03-27 22:55:33'),(10,'defaultcredits','5','2013-10-02 13:28:21','2018-10-04 21:02:08'),(11,'isenabled','-1','2013-10-02 13:28:21','2014-03-24 15:49:09'),(12,'iscredits','1','2013-10-02 13:32:59','2018-10-04 21:02:08'),(13,'isspotlight','0','2013-10-02 14:01:49','2014-03-28 10:53:42'),(14,'issuperpower','0','2013-10-02 14:01:49','2014-03-28 10:53:42'),(15,'theme','metronic','2013-10-03 16:28:41','2013-10-03 16:41:22'),(16,'paypalusername','','2013-10-05 08:43:12','2014-03-29 22:34:51'),(17,'paypalidentitytoken','','2013-10-05 08:43:12','2013-12-25 15:56:58'),(46,'email_notification_disable_user','1','2013-11-06 23:59:16','2014-03-28 12:16:43'),(22,'encounters_without_pic','-1','2013-10-18 02:06:06','2014-03-24 15:49:09'),(23,'email_host','smtp.gmail.com.','2013-10-28 12:24:44','2018-10-04 20:49:49'),(24,'email_port','587','2013-10-28 12:24:44','2018-10-04 20:49:49'),(25,'email_username','jefferyphillps1005@gmail.com','2013-10-28 12:24:44','2018-10-04 20:49:49'),(26,'email_password','qkwkrdlfgodwyd!','2013-10-28 12:24:44','2018-10-04 20:49:49'),(27,'email_encryption','tls','2013-10-28 12:24:44','2018-10-04 20:36:42'),(28,'email_notification_profile_visitor','1','2013-10-28 13:12:12','2014-03-28 12:16:43'),(31,'email_notification_meetme','1','2013-10-28 13:12:51','2014-03-28 12:16:43'),(30,'email_notification_message','1','2013-10-28 13:12:12','2014-03-28 12:16:52'),(32,'email_content_profile_visitor','[from_username] just visited your profile.\r\n\r\nCheckout their profile -> [from_user_profile_link]','2013-10-28 15:21:29','2014-03-28 08:54:22'),(33,'email_content_message','[from_username] sent you a message!\r\n\r\n[message]','2013-10-29 08:19:25','2014-03-28 10:58:40'),(34,'email_content_meetme','[from_username] wants to meet you.\r\n\r\nThis is the profile  [from_user_profile_link]','2013-10-29 08:19:31','2014-03-28 09:47:35'),(35,'email_content_forgot_password','','2013-10-29 08:19:38','2013-10-29 08:19:38'),(36,'email_content_email_verification','Verify your email\r\n\r\n[verification_no]','2013-10-29 08:19:43','2014-03-29 09:57:04'),(37,'widget_profile_right_1','','2013-10-29 13:06:59','2013-10-29 13:35:07'),(38,'widget_profile_right_2','','2013-10-29 13:06:59','2013-10-29 13:35:07'),(39,'widget_profile_right_3','','2013-10-29 13:06:59','2013-10-29 13:35:07'),(40,'widget_encounter_right_1','','2013-10-29 13:06:59','2013-10-29 13:47:37'),(41,'widget_encounter_right_2','','2013-10-29 13:06:59','2013-10-29 13:47:37'),(42,'widget_above_spot_light','','2013-10-29 13:06:59','2013-10-29 15:05:47'),(43,'widget_below_left_side_menu','','2013-10-29 13:06:59','2013-10-29 15:05:47'),(45,'from_email','info@dateme.com','2013-10-30 12:04:20','2018-10-04 20:17:53'),(47,'email_notification_delete_photo','0','2013-11-06 23:59:16','2013-11-28 17:15:28'),(48,'email_notification_delete_user','1','2013-11-07 00:26:07','2014-03-28 12:16:43'),(49,'email_notification_profile_visitor','-1','2013-11-07 04:29:56','2014-03-24 15:49:09'),(50,'email_notification_want_to_meet','-1','2013-11-07 04:29:56','2014-03-24 15:49:09'),(51,'email_notification_message','-1','2013-11-07 04:29:56','2014-03-24 15:49:09'),(52,'email_notification_profile_visitor','-1','2013-11-07 04:30:33','2014-03-24 15:49:09'),(53,'email_notification_want_to_meet','-1','2013-11-07 04:30:33','2014-03-24 15:49:09'),(54,'email_notification_message','-1','2013-11-07 04:30:33','2014-03-24 15:49:09'),(55,'email_notification_profile_visitor','0','2013-11-07 04:30:46','2013-11-07 04:30:46'),(56,'email_notification_message','0','2013-11-07 04:30:46','2013-11-07 04:30:46'),(57,'email_notification_want_to_meet','0','2013-11-07 04:30:46','2013-11-07 04:30:46'),(58,'email_notification_profile_visitor','-1','2013-11-07 04:32:05','2014-03-24 15:49:09'),(59,'email_notification_want_to_meet','-1','2013-11-07 04:32:05','2014-03-24 15:49:09'),(60,'email_notification_message','-1','2013-11-07 04:32:05','2014-03-24 15:49:09'),(61,'email_notification_profile_visitor','0','2013-11-07 04:32:09','2013-11-07 04:32:09'),(62,'email_notification_message','-1','2013-11-07 04:32:09','2014-03-24 15:49:09'),(63,'email_notification_want_to_meet','-1','2013-11-07 04:32:09','2014-03-24 15:49:09'),(64,'email_notification_profile_visitor','-1','2013-11-07 04:32:09','2014-03-24 15:49:09'),(65,'email_notification_want_to_meet','-1','2013-11-07 04:32:09','2014-03-24 15:49:09'),(66,'email_notification_message','-1','2013-11-07 04:32:09','2014-03-24 15:49:09'),(67,'email_notification_profile_visitor','-1','2013-11-07 04:32:18','2014-03-24 15:49:09'),(68,'email_notification_message','-1','2013-11-07 04:32:18','2014-03-24 15:49:09'),(69,'email_notification_want_to_meet','0','2013-11-07 04:32:18','2013-11-07 04:32:18'),(70,'email_notification_profile_visitor','-1','2013-11-07 04:32:18','2014-03-24 15:49:09'),(71,'email_notification_want_to_meet','-1','2013-11-07 04:32:18','2014-03-24 15:49:09'),(72,'email_notification_message','-1','2013-11-07 04:32:18','2014-03-24 15:49:09'),(73,'email_notification_send_message_request','-1','2013-11-07 06:36:27','2014-03-24 15:49:09'),(74,'email_notification_accept_message_request','-1','2013-11-07 06:36:27','2014-03-24 15:49:09'),(75,'isrewards','1','2013-11-07 11:57:32','2014-03-28 10:54:37'),(76,'istopup','-1','2013-11-07 12:35:40','2014-03-24 15:49:09'),(78,'encounters_min_album_pics','0','2013-11-17 09:38:29','2013-11-17 09:38:29'),(79,'photorater_min_photo_to_rate','0','2013-11-17 09:38:29','2013-11-17 09:38:29'),(80,'photorater_min_photo_to_get_rated','-1','2013-11-17 09:38:29','2014-03-24 15:49:09'),(81,'email_notification_comment_photo','1','2013-11-17 12:07:28','2014-03-28 12:16:43'),(82,'email_notification_rate_photo','1','2013-11-17 12:07:28','2014-03-28 12:16:43'),(83,'email_content_rate_photo','[from_username] rated your photo','2013-11-17 12:12:48','2014-03-29 16:56:20'),(84,'email_subject_rate_photo','Rated your photo','2013-11-17 12:12:48','2014-03-29 16:56:20'),(85,'email_content_comment_photo','','2013-11-17 12:12:55','2013-11-17 12:12:55'),(86,'email_subject_comment_photo','','2013-11-17 12:12:55','2013-11-17 12:12:55'),(104,'search_engine_access','0','2013-12-25 15:52:17','2014-02-22 13:05:41'),(87,'email_content_delete_user','','2013-11-28 16:02:03','2013-11-28 16:02:03'),(88,'email_subject_delete_user','','2013-11-28 16:02:03','2013-11-28 16:02:03'),(89,'email_content_disable_user','','2013-11-28 16:09:26','2013-11-28 16:09:26'),(90,'email_subject_disable_user','','2013-11-28 16:09:26','2013-11-28 16:09:26'),(91,'email_subject_email_verification','Verify Your Email','2013-11-28 17:00:47','2014-03-29 09:57:04'),(92,'email_subject_meetme','Someone wants to meet you','2013-11-28 17:01:12','2014-03-28 09:47:35'),(93,'email_content_accept_message_request','','2013-11-28 17:01:17','2013-11-28 17:01:17'),(94,'email_subject_accept_message_request','','2013-11-28 17:01:17','2013-11-28 17:01:17'),(95,'email_subject_profile_visitor','Someone visited your profile','2013-11-28 17:01:23','2014-03-28 08:54:22'),(96,'email_content_send_message_request','m','2013-11-28 17:01:29','2014-03-09 14:31:46'),(97,'email_subject_send_message_request','','2013-11-28 17:01:29','2013-11-28 17:01:29'),(98,'email_content_delete_photo','','2013-11-28 17:08:45','2013-11-28 17:08:45'),(99,'email_subject_delete_photo','','2013-11-28 17:08:45','2013-11-28 17:08:45'),(100,'email_subject_forgot_password','','2013-11-28 17:14:57','2013-11-28 17:14:57'),(102,'meta_keywords','love, date, social network, meet, nixby, social, music,','2013-12-25 15:20:59','2018-10-04 21:01:49'),(103,'meta_description','','2013-12-25 15:20:59','2013-12-25 15:56:58'),(118,'favicon','0','2014-02-22 12:39:25','2014-03-29 22:34:31'),(114,'debug_mode','0','2014-02-22 11:15:37','2018-10-04 21:00:44'),(119,'description','The free social network to find love! Join us free today.','2014-02-22 13:05:14','2018-10-04 21:01:49'),(120,'google_ua','','2014-02-23 16:13:16','2014-02-23 16:13:21'),(121,'spotlight_cost','40','2014-02-23 17:50:16','2014-02-23 17:50:16'),(122,'riseup_cost','20','2014-02-23 17:50:16','2018-10-04 21:02:23'),(123,'superpower_cost','30','2014-02-23 17:50:16','2014-02-23 17:50:16'),(124,'user_languages','{\"ar\":1,\"bg\":1,\"de\":1,\"en\":1,\"es\":1,\"fr\":1,\"he\":1,\"hu\":1,\"it\":1,\"pt\":1,\"ru\":1}','2014-03-09 09:08:37','2018-10-04 19:53:05'),(125,'default_language','en','2014-03-09 00:00:00','2014-03-29 22:24:52'),(126,'facebook_share','0','2014-03-09 15:00:48','2014-03-29 22:36:03'),(127,'no_bot','5','2014-03-12 21:59:37','2018-10-04 22:28:43'),(128,'bot_gender','0','2014-03-12 21:59:37','2014-03-29 22:37:04'),(129,'banner_top_bar','-1','2014-03-24 15:49:01','2014-03-24 15:56:51'),(130,'banner_bottom_bar','-1','2014-03-24 15:49:01','2014-03-24 15:49:01'),(131,'banner_left_side_bar','-1','2014-03-24 15:49:01','2014-03-24 15:49:01'),(141,'email_notification_mutual_attraction','1','2014-03-28 12:16:43','2014-03-28 12:16:43'),(138,'email_content_add_contact','[from_username] added you as contact','2014-03-28 10:58:15','2014-03-28 10:58:15'),(132,'show_superpowers','1','2014-03-24 16:32:05','2014-03-28 10:05:51'),(133,'show_riseup_msg','1','2014-03-24 16:32:05','2014-03-28 10:05:51'),(134,'show_fb_invite','1','2014-03-24 16:32:05','2014-03-28 10:05:51'),(135,'show_photo_rater','1','2014-03-24 16:32:05','2014-03-24 16:32:05'),(136,'show_encounters','1','2014-03-24 16:32:05','2014-03-24 16:32:05'),(137,'frontbackgroundimage','','2014-03-27 20:54:25','2014-03-29 22:37:12'),(139,'email_subject_add_contact','Added you as contact','2014-03-28 10:58:15','2014-03-28 10:58:15'),(140,'email_subject_message','Sent you a message','2014-03-28 10:58:40','2014-03-28 10:58:40'),(142,'email_notification_add_contact','1','2014-03-28 12:16:43','2014-03-28 12:16:43'),(143,'email_notification_send_gift','1','2014-03-28 12:16:43','2014-03-28 12:16:43'),(144,'email_content_send_gift','[from_username] sent you a gift!','2014-03-29 17:14:58','2014-03-29 17:14:58'),(145,'email_subject_send_gift','You received a gift','2014-03-29 17:14:58','2014-03-29 17:14:58'),(146,'email_content_mutual_attraction','[from_username] wants to meet you too!!','2014-03-29 18:44:00','2014-03-29 18:44:00'),(147,'email_subject_mutual_attraction','Mutual Attraction','2014-03-29 18:45:59','2014-03-29 18:45:59'),(148,'logo','26145bb67f757e68b','2018-10-04 21:00:37','2018-10-04 21:00:37');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spotlights`
--

DROP TABLE IF EXISTS `spotlights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spotlights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spotlights`
--

LOCK TABLES `spotlights` WRITE;
/*!40000 ALTER TABLE `spotlights` DISABLE KEYS */;
/*!40000 ALTER TABLE `spotlights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `superpowers`
--

DROP TABLE IF EXISTS `superpowers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `superpowers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `superpowers`
--

LOCK TABLES `superpowers` WRITE;
/*!40000 ALTER TABLE `superpowers` DISABLE KEYS */;
/*!40000 ALTER TABLE `superpowers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_gifts`
--

DROP TABLE IF EXISTS `user_gifts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_gifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `from_user` int(11) NOT NULL,
  `to_user` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_gifts`
--

LOCK TABLES `user_gifts` WRITE;
/*!40000 ALTER TABLE `user_gifts` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_gifts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_interests`
--

DROP TABLE IF EXISTS `user_interests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_interests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interest_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_interests`
--

LOCK TABLES `user_interests` WRITE;
/*!40000 ALTER TABLE `user_interests` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_interests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_settings`
--

DROP TABLE IF EXISTS `user_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `value` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_settings`
--

LOCK TABLES `user_settings` WRITE;
/*!40000 ALTER TABLE `user_settings` DISABLE KEYS */;
INSERT INTO `user_settings` VALUES (20,21095,'show_me_offline','0','2018-10-04 19:53:04','2018-10-04 19:53:04'),(21,21095,'hide_from_search','0','2018-10-04 19:53:04','2018-10-04 19:53:04'),(22,21095,'send_add_contact_email','1','2018-10-04 19:53:04','2018-10-04 19:53:04'),(23,21095,'send_meet_me_email','1','2018-10-04 19:53:04','2018-10-04 19:53:04'),(24,21095,'send_photo_commented_email','1','2018-10-04 19:53:04','2018-10-04 19:53:04'),(25,21095,'send_photo_rated_email','1','2018-10-04 19:53:04','2018-10-04 19:53:04'),(26,21095,'send_message_sent_email','1','2018-10-04 19:53:04','2018-10-04 19:53:04'),(27,21095,'send_profile_visitor_email','1','2018-10-04 19:53:04','2018-10-04 19:53:04'),(28,21095,'send_gift_sent_email','1','2018-10-04 19:53:04','2018-10-04 19:53:04'),(29,21095,'send_mutual_attraction_email','1','2018-10-04 19:53:04','2018-10-04 19:53:04'),(30,21109,'show_me_offline','0','2018-10-04 20:55:13','2018-10-04 20:55:13'),(31,21109,'hide_from_search','0','2018-10-04 20:55:13','2018-10-04 20:55:13'),(32,21109,'send_add_contact_email','1','2018-10-04 20:55:13','2018-10-04 20:55:13'),(33,21109,'send_meet_me_email','1','2018-10-04 20:55:13','2018-10-04 20:55:13'),(34,21109,'send_photo_commented_email','1','2018-10-04 20:55:13','2018-10-04 20:55:13'),(35,21109,'send_photo_rated_email','1','2018-10-04 20:55:13','2018-10-04 20:55:13'),(36,21109,'send_message_sent_email','1','2018-10-04 20:55:13','2018-10-04 20:55:13'),(37,21109,'send_profile_visitor_email','1','2018-10-04 20:55:13','2018-10-04 20:55:13'),(38,21109,'send_gift_sent_email','1','2018-10-04 20:55:13','2018-10-04 20:55:13'),(39,21109,'send_mutual_attraction_email','1','2018-10-04 20:55:13','2018-10-04 20:55:13'),(40,21110,'show_me_offline','0','2018-10-04 20:57:38','2018-10-04 20:57:38'),(41,21110,'hide_from_search','0','2018-10-04 20:57:38','2018-10-04 20:57:38'),(42,21110,'send_add_contact_email','1','2018-10-04 20:57:38','2018-10-04 20:57:38'),(43,21110,'send_meet_me_email','1','2018-10-04 20:57:38','2018-10-04 20:57:38'),(44,21110,'send_photo_commented_email','1','2018-10-04 20:57:38','2018-10-04 20:57:38'),(45,21110,'send_photo_rated_email','1','2018-10-04 20:57:38','2018-10-04 20:57:38'),(46,21110,'send_message_sent_email','1','2018-10-04 20:57:38','2018-10-04 20:57:38'),(47,21110,'send_profile_visitor_email','1','2018-10-04 20:57:38','2018-10-04 20:57:38'),(48,21110,'send_gift_sent_email','1','2018-10-04 20:57:38','2018-10-04 20:57:38'),(49,21110,'send_mutual_attraction_email','1','2018-10-04 20:57:38','2018-10-04 20:57:38'),(50,21111,'show_me_offline','0','2018-10-05 08:21:17','2018-10-05 08:21:17'),(51,21111,'hide_from_search','0','2018-10-05 08:21:17','2018-10-05 08:21:17'),(52,21111,'send_add_contact_email','1','2018-10-05 08:21:17','2018-10-05 08:21:17'),(53,21111,'send_meet_me_email','1','2018-10-05 08:21:17','2018-10-05 08:21:17'),(54,21111,'send_photo_commented_email','1','2018-10-05 08:21:17','2018-10-05 08:21:17'),(55,21111,'send_photo_rated_email','1','2018-10-05 08:21:17','2018-10-05 08:21:17'),(56,21111,'send_message_sent_email','1','2018-10-05 08:21:17','2018-10-05 08:21:17'),(57,21111,'send_profile_visitor_email','1','2018-10-05 08:21:17','2018-10-05 08:21:17'),(58,21111,'send_gift_sent_email','1','2018-10-05 08:21:17','2018-10-05 08:21:17'),(59,21111,'send_mutual_attraction_email','1','2018-10-05 08:21:17','2018-10-05 08:21:17'),(60,21112,'show_me_offline','0','2018-10-05 08:39:32','2018-10-05 08:39:32'),(61,21112,'hide_from_search','0','2018-10-05 08:39:32','2018-10-05 08:39:32'),(62,21112,'send_add_contact_email','1','2018-10-05 08:39:32','2018-10-05 08:39:32'),(63,21112,'send_meet_me_email','1','2018-10-05 08:39:32','2018-10-05 08:39:32'),(64,21112,'send_photo_commented_email','1','2018-10-05 08:39:32','2018-10-05 08:39:32'),(65,21112,'send_photo_rated_email','1','2018-10-05 08:39:32','2018-10-05 08:39:32'),(66,21112,'send_message_sent_email','1','2018-10-05 08:39:32','2018-10-05 08:39:32'),(67,21112,'send_profile_visitor_email','1','2018-10-05 08:39:32','2018-10-05 08:39:32'),(68,21112,'send_gift_sent_email','1','2018-10-05 08:39:32','2018-10-05 08:39:32'),(69,21112,'send_mutual_attraction_email','1','2018-10-05 08:39:32','2018-10-05 08:39:32');
/*!40000 ALTER TABLE `user_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `gender` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `photo_id` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `role` int(11) NOT NULL,
  `city` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `country` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `verified` int(11) NOT NULL,
  `verification_no` varchar(16) CHARACTER SET latin1 NOT NULL,
  `language` varchar(3) CHARACTER SET latin1 NOT NULL,
  `profile_score` int(11) NOT NULL,
  `album_count` int(11) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21113 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (21111,'craigjames92@hotmail.co.uk','Craig','$2a$08$mNWwAVsL1bcT3IdJgv2LfOZKiqQlBp/lzWTQsabLQWwdExHWy2o5K',1,23,'2018-10-05 08:21:15','2018-10-05 08:21:15','2018-10-05 08:21:15','0',0,'swansea','',NULL,0,'5bb71efba6c95','',0,0,0.000000,0.000000),(21112,'james@bremak.co.ke','James Makau','$2a$08$icSYx30gEpre9RWnq11XtuNjGyrwKwugqrzpbOdrafCkYe2BvBCxe',1,31,'2018-10-05 09:24:59','2018-10-05 08:39:31','2018-10-05 09:24:59','0',0,'Nairobi','',NULL,1,'5bb72343a314b','',9,0,0.000000,0.000000),(21110,'mrccook2017@gmail.com','Craig','$2a$08$TWYZ3i.V2l0easQyLdZVfeDruy9nCixdcsV3pmHGqESGCdKffpv9u',1,25,'2018-10-05 08:18:41','2018-10-04 20:57:37','2018-10-05 08:18:41','0',0,'swansea','',NULL,1,'5bb67ec147e04','',0,0,0.000000,0.000000);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-05  9:36:42
